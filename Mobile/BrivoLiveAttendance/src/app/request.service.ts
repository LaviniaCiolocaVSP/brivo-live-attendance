import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Door} from "./Door";

@Injectable({
    providedIn: 'root'
})
export class RequestService {

    url = 'http://192.168.0.108:8080';

    constructor(
        public httpClient: HttpClient
    ) {
    }

    getRoom(): Promise<any> {
        return this.httpClient.get(this.url + '/room').toPromise();
    }

    getLogs(): Promise<any> {
        return this.httpClient.get(this.url + '/logs').toPromise();
    }

    getDoors(): Promise<any> {
        return this.httpClient.get(this.url + '/doors').toPromise();
    }

    updateDoors(doors: Door[]): Promise<any> {
        console.log(doors);

        let _headers: HttpHeaders = new HttpHeaders();
        _headers = _headers.append('Content-Type', 'application/json');
        _headers = _headers.append('Access-Control-Allow-Headers', 'Content-Type, Accept');
        return this.httpClient.post(this.url + '/doors', doors, {headers: _headers}).toPromise();
    }
}
