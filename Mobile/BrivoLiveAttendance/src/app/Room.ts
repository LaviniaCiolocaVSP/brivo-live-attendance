export class Room {
    name: string;
    color: string;
    currentOccupancy: number;
    maxCapacity: number;
    unauthorizedAccess: string;
}
