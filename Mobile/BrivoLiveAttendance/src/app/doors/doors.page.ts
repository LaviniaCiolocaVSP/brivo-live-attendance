import {Component, OnInit} from '@angular/core';
import {Door} from "../Door";
import {RequestService} from "../request.service";
import {Router} from "@angular/router";
import {ToastController} from "@ionic/angular";

@Component({
    selector: 'app-doors',
    templateUrl: './doors.page.html',
    styleUrls: ['./doors.page.scss'],
})
export class DoorsPage {

    doors: Door[] = [];
    toast: any;

    constructor(public requestService: RequestService,
                public router: Router, public toastController: ToastController) {

    }

    getDoorsInfo() {
        this.requestService.getDoors().then((result) => {
            this.doors = result;
            console.log(this.doors);
        }).catch((error) => {
            console.log(error);
            this.showToast('Something went wrong! Please try again!', 'danger');
        });
    }

    goBack() {
        this.router.navigate(['/home']);
    }

    checkAndSave() {
        console.log(this.doors);
        let inDoors = 0;
        let outDoors = 0;
        for (const door of this.doors) {
            if (door.notEnter) {
                outDoors++;
            } else {
                inDoors++;
            }
        }

        if (inDoors > 0 && outDoors > 0) {
            this.requestService.updateDoors(this.doors).then((result) => {
                this.showToast('Settings were saved successfully', 'success');
            }).catch((error) => {
                this.showToast('Something went wrong! Please try again!', 'danger');
            });

        } else {
            this.showToast('At least one door of each type has to be defined', 'danger');

        }
    }

    showToast(toastMessage: string, toastColor: string) {
        this.toast = this.toastController.create({
            message: toastMessage,
            duration: 3000,
            color: toastColor,
            position: 'top'
        }).then((toastData) => {
            console.log(toastData);
            toastData.present();
        });
    }

    ionViewDidEnter() {
        this.getDoorsInfo();
    }
}
