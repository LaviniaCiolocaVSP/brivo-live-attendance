import {Component, OnInit} from '@angular/core';
import {RequestService} from '../request.service';
import {Router} from '@angular/router';
import {Logs} from '../Logs';

@Component({
    selector: 'app-logs',
    templateUrl: './logs.page.html',
    styleUrls: ['./logs.page.scss'],
})
export class LogsPage {

    private interval: any;
    private readonly refreshRate = 2000;
    private logs: Logs[] = [];

    constructor(
        public requestService: RequestService,
        public router: Router
    ) {
        this.getData();
    }

    getData() {
        this.refreshData();

        if (typeof this.interval !== 'undefined') {
            clearInterval(this.interval);
        }

        this.interval = setInterval(_ => {
            this.refreshData();
        }, this.refreshRate);
    }

    refreshData() {
        this.requestService.getLogs().then((data) => {
            this.logs = data;
            console.log(this.logs);
        }).catch(error => {
            console.log('Error occurred while making the HTTP request');
            console.log(error);
        });
    }

    goBack() {
        this.router.navigate(['/home']);
    }

    ionViewWillLeave() {
        if (typeof this.interval !== 'undefined') {
            clearInterval(this.interval);
        }
    }

    ionViewDidEnter() {
        this.getData();
    }

}
