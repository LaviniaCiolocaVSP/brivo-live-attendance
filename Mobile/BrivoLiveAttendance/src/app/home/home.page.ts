/* tslint:disable */
import {Component} from '@angular/core';
import {RequestService} from '../request.service';
import {Room} from '../Room';
import {Router} from '@angular/router';
import {ToastController} from "@ionic/angular";

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    private room: Room = new Room();
    private interval;
    private readonly refreshRate = 2000;
    toast: any;

    constructor(
        public requestService: RequestService,
        public router: Router,
        public toastController: ToastController
    ) {
        this.getData();
    }

    getData() {
        this.refreshData();

        if (typeof this.interval != 'undefined') {
            clearInterval(this.interval);
        }

        this.interval = setInterval(_ => {
            this.refreshData();
        }, this.refreshRate);
    }


    ionViewDidEnter() {
        this.getData();
    }

    ionViewWillLeave() {
        if (typeof this.interval != 'undefined') {
            clearInterval(this.interval);
        }
    }


    refreshData() {
        this.requestService.getRoom().then((data) => {
            this.room.name = data.name;
            this.room.color = data.color;
            this.room.currentOccupancy = data.currentOccupancy;
            this.room.maxCapacity = data.maxCapacity;
            this.room.unauthorizedAccess = data.unauthorizedAccess;
            console.log(this.room);
            this.computeCircleDiameter();

            if (this.room.unauthorizedAccess != null && this.room.unauthorizedAccess != undefined)
                this.showToast(this.room.unauthorizedAccess);

        }).catch(error => {
            console.log("Error occurred while making the HTTP request");
            console.log(error);
        });
    }

    computeCircleDiameter() {
        const basePercentToAdd = 50.0; // because the minimum circle diameter is 50%;
        const currentAttendancePercent = this.room.currentOccupancy * (100.0) / this.room.maxCapacity; // * (100.0) so JS recognises this as a float division
        const percentToAdd = basePercentToAdd * currentAttendancePercent / 100.0;
        this.changeCircleDiameter(Math.floor(basePercentToAdd + percentToAdd) + "%");
    }

    changeCircleDiameter(percent: string) {
        document.documentElement.style.setProperty(`--diameter`, percent);
    }

    goToLogsPage() {
        this.router.navigate(['/logs']);
    }

    showToast(toastMessage: string) {
        this.toast = this.toastController.create({
            message: toastMessage,
            duration: 5000,
            color: 'danger',
            position: 'top'
        }).then((toastData) => {
            console.log(toastData);
            toastData.present();
        });
    }

    goToSettingsPage() {
        this.router.navigate(['/doors']);
    }
}
