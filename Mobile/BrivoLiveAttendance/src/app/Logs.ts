export class Logs {
    enterEvent: boolean;
    validEvent: boolean;
    message: string;
    eventDate: string;
}
