package com.live.attendance.demo.models;

import org.springframework.stereotype.Component;

@Component
public class Door {
    private int id;
    private String name;
    private boolean notEnter;

    public Door() {
    }

    public Door(int id, String name, boolean notEnter) {
        this.id = id;
        this.name = name;
        this.notEnter = notEnter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNotEnter() {
        return notEnter;
    }

    public void setNotEnter(boolean notEnter) {
        this.notEnter = notEnter;
    }
}
