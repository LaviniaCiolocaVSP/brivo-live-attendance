package com.live.attendance.demo.controllers;

import com.live.attendance.demo.DoorUtils;
import com.live.attendance.demo.models.Room;
import com.live.attendance.demo.models.SecurityEvent;
import com.live.attendance.demo.repositories.SecurityEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.util.List;

@Controller
public class RoomController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoomController.class);

    @Value("${room.maximumCapacity}")
    private int maximumCapacity;

    @Value("${room.name}")
    private String roomName;

    @Value("${unauthorizedEventCheckTimeSeconds}")
    private int unauthorizedAccessCheckTimeSeconds;

    private static final String UNAUTHORIZED_ACCESS_WARNING_MESSAGE = "An unauthorized attempt was made to access door ";

    private String[] colors = new String[]{"#01496f", "#077ab7", "#00c9ff", "#ffea00", "#bfff00", "#4dff00", "#00a61c"};
    private int colorIndex = 3;
    private int colorStep;
    private int occupancy;

    private String unauthorizedAccess;

    @Autowired
    private DoorUtils doorUtils;

    @Autowired
    private SecurityEventRepository eventRepository;

    @Autowired
    private ServletContext servletContext;

    @PostConstruct
    public void init() {
        this.colorStep = maximumCapacity / 10 == 0 ? 1 : maximumCapacity / 10;
    }

    @GetMapping("/room")
    @ResponseBody
    public Room getRoom() {
        LOGGER.info("Called endpoint /room");

        try {
            doorUtils.refreshDoors();
        } catch (Exception e) {
            e.printStackTrace();
        }

        long appStartTimestamp = (long) servletContext.getAttribute("appStartTime");
        List<SecurityEvent> securityEventList = eventRepository.findAllByEventTypeEqualsAndOccurredMillisAfter("PANEL_EVENT", appStartTimestamp);
        long currentMillis = System.currentTimeMillis();
        getOccupancyFromEventList(securityEventList, currentMillis);

        Room toReturn = new Room(roomName, colors[colorIndex], occupancy, maximumCapacity, unauthorizedAccess);
        LOGGER.info("Returning room {}", toReturn);
        return toReturn;
    }

    private void getOccupancyFromEventList(List<SecurityEvent> securityEventList, Long currentMillis) {
        // this sorts the list in ascending order based on the timestamp
        securityEventList.sort((securityEvent, t1) ->
                                       (int) (securityEvent.getOccurredMillis() - t1.getOccurredMillis()));

        this.occupancy = 0;
        colorIndex = 3;
        unauthorizedAccess = null;
        for (SecurityEvent event : securityEventList) {
            if (isAValidEvent(event)) {
                if (isAnEnterEvent(event)) {
                    this.occupancy++;

                    if (this.occupancy % this.colorStep == 0 && colorIndex + 1 != this.colors.length) {
                        colorIndex++;
                    }
                } else {
                    if (isAnExitEvent(event)) {
                        this.occupancy--;

                        if (this.occupancy % this.colorStep == 0 && colorIndex != 0) {
                            colorIndex--;
                        }
                    } else {
                        LOGGER.info("An invalid event was detected: {}", event);
                    }
                }
            } else if (isAnUnauthorizedEvent(event) && event.getOccurredMillis() > currentMillis - unauthorizedAccessCheckTimeSeconds * 1000) {
                if (isAnEnterEvent(event))
                    unauthorizedAccess = UNAUTHORIZED_ACCESS_WARNING_MESSAGE + doorUtils.getDoorEnter();
                else if (isAnExitEvent(event))
                    unauthorizedAccess = UNAUTHORIZED_ACCESS_WARNING_MESSAGE + doorUtils.getDoorExit();
            }
        }
    }

    private boolean isAValidEvent(SecurityEvent event) {
        // 2004 being the Open the device (door) event
        return event.getSecurityActionId() == 2004;
    }

    private boolean isAnEnterEvent(SecurityEvent event) {
        // here we should check if the door from which the event came is the enter door
        return event.getObjectId() == doorUtils.getDoorEnter();
    }

    private boolean isAnExitEvent(SecurityEvent event) {
        // here we should check if the door from which the event came is the enter door
        return event.getObjectId() == doorUtils.getDoorExit();
    }

    private boolean isAnUnauthorizedEvent(SecurityEvent event) {
        //here we should check if it was an unauthorized event
        return event.getSecurityActionId() >= 5012 && event.getSecurityActionId() <= 5018;
    }
}
