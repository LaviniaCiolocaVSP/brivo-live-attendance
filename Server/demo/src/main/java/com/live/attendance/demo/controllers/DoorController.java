package com.live.attendance.demo.controllers;

import com.live.attendance.demo.DoorUtils;
import com.live.attendance.demo.config.DoorEnum;
import com.live.attendance.demo.models.Door;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@RestController
public class DoorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoorController.class);

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private DoorUtils doorUtils;

    @GetMapping("/doors")
    @ResponseBody
    public List<Door> getDoors() {
        LOGGER.info("Called endpoint GET /doors");

        try {
            doorUtils.refreshDoors();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Door enterDoor = new Door(doorUtils.getDoorEnter(), DoorEnum.getName(doorUtils.getDoorEnter()), false);
        Door exitDoor = new Door(doorUtils.getDoorExit(), DoorEnum.getName(doorUtils.getDoorExit()), true);

        List<Door> doors = new ArrayList<>();
        doors.add(enterDoor);
        doors.add(exitDoor);

        return doors;
    }

    @PostMapping(value = "/doors", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String postDoors(@RequestBody List<Door> doors) throws Exception {
        LOGGER.info("Called endpoint POST /doors");

        FileInputStream in = new FileInputStream(new File("src\\main\\resources\\application.properties"));
        Properties props = new Properties();
        props.load(in);
        in.close();

        FileOutputStream out = new FileOutputStream(new File("src\\main\\resources\\application.properties"));

        for (Door door : doors) {
            if (door.isNotEnter()) {
                props.setProperty("door.exit", String.valueOf(door.getId()));
            } else {
                props.setProperty("door.enter", String.valueOf(door.getId()));
            }
        }

        props.store(out, null);
        out.close();

        servletContext.setAttribute("appStartTime", System.currentTimeMillis());

        return "";
    }
}
