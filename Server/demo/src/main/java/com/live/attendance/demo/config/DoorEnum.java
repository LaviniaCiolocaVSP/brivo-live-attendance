package com.live.attendance.demo.config;

public enum DoorEnum {
    DOOR1(60232, "Door SCP-000FE5057D49"),
    DOOR2(60208, "Door 2 SCP-000FE5057D49");

    private int id;
    private String name;

    DoorEnum(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static String getName(int id) {
        for (DoorEnum door : values()) {
            if (door.id == id) {
                return door.name;
            }
        }
        return "";
    }
}
