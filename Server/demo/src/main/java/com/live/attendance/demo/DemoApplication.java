package com.live.attendance.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import javax.servlet.ServletContext;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Autowired
    private ServletContext servletContext;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) {
        servletContext.setAttribute("appStartTime", System.currentTimeMillis());
    }
}

