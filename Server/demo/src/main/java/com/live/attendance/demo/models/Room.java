package com.live.attendance.demo.models;

public class Room {
    private String name;
    private String color;
    private int currentOccupancy;
    private int maxCapacity;
    private String unauthorizedAccess;

    public Room(String name, String color, int currentOccupancy, int maxCapacity, String unauthorizedAccess) {
        this.name = name;
        this.color = color;
        this.currentOccupancy = currentOccupancy;
        this.maxCapacity = maxCapacity;
        this.unauthorizedAccess = unauthorizedAccess;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCurrentOccupancy() {
        return currentOccupancy;
    }

    public void setCurrentOccupancy(int currentOccupancy) {
        this.currentOccupancy = currentOccupancy;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getUnauthorizedAccess() {
        return unauthorizedAccess;
    }

    public void setUnauthorizedAccess(String unauthorizedAccess) {
        this.unauthorizedAccess = unauthorizedAccess;
    }


    @Override
    public String toString() {
        return "Room{" +
               "name='" + name + '\'' +
               ", color='" + color + '\'' +
               ", currentOccupancy=" + currentOccupancy +
               ", maxCapacity=" + maxCapacity +
               ", unauthorizedAccess='" + unauthorizedAccess + '\'' +
               '}';
    }
}
