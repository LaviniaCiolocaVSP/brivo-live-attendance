package com.live.attendance.demo.models;

public class Logs {
    private boolean enterEvent;
    private boolean validEvent;
    private String message;
    private String eventDate;

    public Logs() {
    }

    public Logs(boolean isEnterEvent, boolean isValidEvent, String message, String eventDate) {
        this.enterEvent = isEnterEvent;
        this.validEvent = isValidEvent;
        this.message = message;
        this.eventDate = eventDate;
    }

    public boolean isEnterEvent() {
        return enterEvent;
    }

    public void setEnterEvent(boolean enterEvent) {
        this.enterEvent = enterEvent;
    }

    public boolean isValidEvent() {
        return validEvent;
    }

    public void setValidEvent(boolean validEvent) {
        this.validEvent = validEvent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }
}
