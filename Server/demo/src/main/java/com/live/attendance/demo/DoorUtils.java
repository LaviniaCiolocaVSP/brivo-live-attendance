package com.live.attendance.demo;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

@Service
public class DoorUtils {

    private int doorEnter;
    private int doorExit;

    public void refreshDoors() throws Exception {
        FileInputStream in = new FileInputStream(new File("src\\main\\resources\\application.properties"));
        Properties props = new Properties();
        props.load(in);
        in.close();

        this.doorEnter = Integer.parseInt(props.getProperty("door.enter"));
        this.doorExit = Integer.parseInt(props.getProperty("door.exit"));
    }

    public int getDoorEnter() {
        return doorEnter;
    }

    public int getDoorExit() {
        return doorExit;
    }
}
