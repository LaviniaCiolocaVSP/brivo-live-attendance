package com.live.attendance.demo.repositories;

import com.live.attendance.demo.models.SecurityEvent;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SecurityEventRepository extends CassandraRepository<SecurityEvent, String> {

    @Override
    List<SecurityEvent> findAll();

    @AllowFiltering
    List<SecurityEvent> findAllByEventTypeEqualsAndOccurredMillisAfter(String eventType, Long occurredMillisAfter);
}
