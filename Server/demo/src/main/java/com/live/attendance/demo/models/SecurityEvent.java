package com.live.attendance.demo.models;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("security_event")
public class SecurityEvent {

    @PrimaryKeyColumn(name = "account_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private Long accountId;

    @PrimaryKeyColumn(name = "event_day", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private Long eventDay;

    @PrimaryKeyColumn(name = "object_group_oid", ordinal = 2, type = PrimaryKeyType.PARTITIONED)
    private Long objectGroupOid;

    @PrimaryKeyColumn(name = "actor_object_id", ordinal = 3, type = PrimaryKeyType.PARTITIONED)
    private Long actorObjectId;

    @PrimaryKeyColumn(name = "object_id", ordinal = 4, type = PrimaryKeyType.PARTITIONED)
    private Long objectId;

    @PrimaryKeyColumn(name = "security_action_id", ordinal = 5, type = PrimaryKeyType.PARTITIONED)
    private Long securityActionId;

    @PrimaryKeyColumn(name = "occurred_millis", ordinal = 6, type = PrimaryKeyType.PARTITIONED)
    private Long occurredMillis;

    @Column("event_data")
    private String eventData;

    @Column("event_type")
    private String eventType;

    @Column("id")
    private String id;

    public SecurityEvent(Long accountId, Long eventDay, Long objectGroupOid, Long actorObjectId, Long objectId, Long securityActionId, Long occurredMillis, String eventData, String eventType, String id) {
        this.accountId = accountId;
        this.eventDay = eventDay;
        this.objectGroupOid = objectGroupOid;
        this.actorObjectId = actorObjectId;
        this.objectId = objectId;
        this.securityActionId = securityActionId;
        this.occurredMillis = occurredMillis;
        this.eventData = eventData;
        this.eventType = eventType;
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getEventDay() {
        return eventDay;
    }

    public void setEventDay(Long eventDay) {
        this.eventDay = eventDay;
    }

    public Long getObjectGroupOid() {
        return objectGroupOid;
    }

    public void setObjectGroupOid(Long objectGroupOid) {
        this.objectGroupOid = objectGroupOid;
    }

    public Long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(Long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(Long securityActionId) {
        this.securityActionId = securityActionId;
    }

    public Long getOccurredMillis() {
        return occurredMillis;
    }

    public void setOccurredMillis(Long occurredMillis) {
        this.occurredMillis = occurredMillis;
    }

    public String getEventData() {
        return eventData;
    }

    public void setEventData(String eventData) {
        this.eventData = eventData;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
