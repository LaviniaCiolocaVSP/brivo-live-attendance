package com.live.attendance.demo.controllers;

import com.live.attendance.demo.DoorUtils;
import com.live.attendance.demo.config.DoorEnum;
import com.live.attendance.demo.models.Logs;
import com.live.attendance.demo.models.Room;
import com.live.attendance.demo.models.SecurityEvent;
import com.live.attendance.demo.repositories.SecurityEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class LogsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogsController.class);

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private SecurityEventRepository eventRepository;

    @Autowired
    private DoorUtils doorUtils;

    private static final int LOGS_LIMIT = 15;

    @GetMapping("/logs")
    @ResponseBody
    public List<Logs> getRoom() {
        LOGGER.info("Called endpoint /logs");

        try {
            doorUtils.refreshDoors();
        } catch (Exception e) {
            e.printStackTrace();
        }

        long appStartTimestamp = (long) servletContext.getAttribute("appStartTime");
        List<SecurityEvent> securityEventList = eventRepository.findAllByEventTypeEqualsAndOccurredMillisAfter("PANEL_EVENT", appStartTimestamp);

        return extractLogs(securityEventList);
    }

    private List<Logs> extractLogs(List<SecurityEvent> events) {
        List<Logs> logs = new ArrayList<>();

        // this sorts the list in descending order based on the timestamp
        events.sort((securityEvent, t1) ->
                            (int) (t1.getOccurredMillis() - securityEvent.getOccurredMillis()));

        // this gets the first 15 elements from the list
        List<SecurityEvent> topEvents = events
                .stream()
                .limit(LOGS_LIMIT)
                .collect(Collectors.toList());

        for (SecurityEvent event : topEvents) {
            Logs log = new Logs();

            if (isAValidEvent(event)) {
                log.setValidEvent(true);
            } else if (isAnUnauthorizedEvent(event)) {
                log.setValidEvent(false);
            } else {
                continue;
            }

            if (isAnEnterEvent(event)) {
                log.setEnterEvent(true);
            } else if (isAnExitEvent(event)) {
                log.setEnterEvent(false);
            } else {
                continue;
            }

            if (log.isValidEvent()) {
                if (isAnEnterEvent(event)) {
                    String doorName = DoorEnum.getName(doorUtils.getDoorEnter());
                    log.setMessage("Authorized enter for " + getActorName(event) + " for door " + doorName.toUpperCase());
                } else if (isAnExitEvent(event)) {
                    String doorName = DoorEnum.getName(doorUtils.getDoorExit());
                    log.setMessage("Authorized exit for " + getActorName(event) + " for door " + doorName.toUpperCase());
                }
            } else {
                if (isAnEnterEvent(event)) {
                    String doorName = DoorEnum.getName(doorUtils.getDoorEnter());
                    log.setMessage("An unauthorized enter attempt was made to door " + doorName.toUpperCase());
                } else if (isAnExitEvent(event)) {
                    String doorName = DoorEnum.getName(doorUtils.getDoorExit());
                    log.setMessage("An unauthorized exit attempt was made to door " + doorName.toUpperCase());
                }
            }

            Date eventDate = new Date(event.getOccurredMillis());
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            log.setEventDate(formatter.format(eventDate));

            logs.add(log);
        }

        return logs;
    }

    private boolean isAValidEvent(SecurityEvent event) {
        // 2004 being the Open the device (door) event
        return event.getSecurityActionId() == 2004;
    }

    private boolean isAnEnterEvent(SecurityEvent event) {
        // here we should check if the door from which the event came is the enter door
        return event.getObjectId() == doorUtils.getDoorEnter();
    }

    private boolean isAnExitEvent(SecurityEvent event) {
        // here we should check if the door from which the event came is the enter door
        return event.getObjectId() == doorUtils.getDoorExit();
    }

    private boolean isAnUnauthorizedEvent(SecurityEvent event) {
        //here we should check if it was an unauthorized event
        return event.getSecurityActionId() >= 5012 && event.getSecurityActionId() <= 5018;
    }

    private String getActorName(SecurityEvent event) {
        String eventData = event.getEventData();
        int testIndex = eventData.indexOf("{\"actorName\":\"");

        if (testIndex != -1) {
            int beginIndex = testIndex + 13;
            int endIndex = eventData.indexOf("\"", beginIndex + 1);
            return eventData.substring(beginIndex + 1, endIndex);
        } else {
            return "";
        }
    }
}
